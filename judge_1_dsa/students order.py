class Node:
    def __init__(self, name, prev = None, next = None):
        self.name = name
        self.next = next
        self.prev = prev
    

class LinkedList:
    def __init__(self):
        self._count = 0
        self._head = None
        self._tail = None

    # destroys the list
    def __str__(self) -> str:
        lst_output = []
        while self._head:
            lst_output.append(self.remove_node(self._head))
        
        return ' '.join(lst_output)

    def insert_before(self, node: Node, new: Node):
        # if not self._head:
        #     raise ValueError('List is empty')
        if node.prev == None:
            new.prev = None
            new.next = self._head
            self._head.prev = new
            self._head = new
            self._count += 1
        else:
            new.prev = node.prev
            new.next = node
            node.prev.next = new
            node.prev = new
            self._count += 1
        
        return new
    
    def add_last_instantiate(self, value):
        if not self._head:
            self._head = self._tail = (new_node := Node(value))
            self._count += 1
        else:
            self._tail.next = (new_node := Node(value, prev = self._tail))
            self._tail = new_node
            self._count += 1
        
        return new_node
    
    def remove_node(self, node: Node):
        # if not self._head:
        #     raise ValueError('List is empty') 
        if self._count == 1:
            self._head = self._tail = None
            self._count = 0
        elif not node.prev:
            self._head = node.next
            self._head.prev = None
            self._count -= 1
        elif not node.next:
            self._tail = node.prev
            self._tail.next = None
            self._count -= 1
        else:
            node.next.prev = node.prev
            node.prev.next = node.next
            self._count -= 1
        
        return node.name

N, K = [int(inp) for inp in input().split()]

lst_names = input().split()

lst_switches_tupled = []
for _ in range(K):
    lst_switches_tupled.append(tuple(input().split()))


linked_list = LinkedList()
dict_obj = {}

for name in lst_names:
    dict_obj[name]: dict[str, Node] = linked_list.add_last_instantiate(name)

for pair in lst_switches_tupled:
    node_1 = dict_obj[pair[0]]
    node_2 = dict_obj[pair[1]]
    linked_list.remove_node(node_1)
    linked_list.insert_before(node_2, node_1)

print(linked_list)