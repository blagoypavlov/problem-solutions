def build_matrix(n, m):
    matrix = [[None]*(m+2)]
    for _ in range(n):
        matrix.append([None] + list(input()) + [None])
    matrix.append([None]*(m+2))
    return matrix

def biggest_field(matrix, r, c):
    count = 0
    if matrix[r][c] == '1':
        count += 1
        matrix[r][c] = None
    else:
        return 0
    count += biggest_field(matrix, r, c - 1)
    count += biggest_field(matrix, r, c + 1)
    count += biggest_field(matrix, r - 1, c)
    count += biggest_field(matrix, r + 1, c)
    
    return count

lst_count = []
n, m = (int(num) for num in input().split())
matrix = build_matrix(n, m)


for r in range(1, n+1):
    for c in range(1, m+1):
        result = biggest_field(matrix, r, c)
        if result != 0:
            lst_count.append(result)

lst_count = sorted(lst_count, reverse=True)

for el in lst_count:
    print(el)