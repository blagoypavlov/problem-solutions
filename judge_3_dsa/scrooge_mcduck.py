def build_matrix(n):
    matrix = []
    for _ in range(n):
        matrix.append([int(num) for num in input().split()])
    return matrix

def in_matrix(matrix, r, c) -> bool:
    rows = len(matrix) - 1
    cols = len(matrix[0]) - 1
    if 0 <= r <= rows and 0 <= c <= cols:
        return True
    return False

def find_start(matrix):
    for r in range(len(matrix)):
        for c in range(len(matrix[0])):
            if matrix[r][c] == 0:
                return r, c
    return None

def move(matrix, start_r, start_c):
    if matrix[start_r][start_c] == 0:
        sum[0] += 0
    else:
        sum[0] += 1
        matrix[start_r][start_c] -= 1
    neighbors = [
        (matrix[start_r][start_c - 1], 4, start_r, start_c - 1) if in_matrix(matrix, start_r, start_c - 1) else (0,), 
        (matrix[start_r][start_c + 1], 3, start_r, start_c + 1) if in_matrix(matrix, start_r, start_c + 1) else (0,), 
        (matrix[start_r - 1][start_c], 2, start_r - 1, start_c) if in_matrix(matrix, start_r - 1, start_c) else (0,), 
        (matrix[start_r + 1][start_c], 1, start_r + 1, start_c) if in_matrix(matrix, start_r + 1, start_c) else (0,)
        ]
    next = max(neighbors)
    if next[0] == 0:
        return None
    else:
        return next


N, _ = (int(num) for num in input().split())
matrix = build_matrix(N)
sum = [0]
start_r, start_c = find_start(matrix)


exec = move(matrix, start_r, start_c)
while exec:
    exec = move(matrix, exec[2], exec[3])

print(sum[0])