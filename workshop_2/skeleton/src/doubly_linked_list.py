from src.linked_list_node import LinkedListNode


class DoublyLinkedList:
    def __init__(self):
        self._head: LinkedListNode = None
        self._tail: LinkedListNode = None
        self._count = 0

    @property
    def count(self):
        return self._count
    
    @property
    def head(self):
        return self._head
    
    @property
    def tail(self):
        return self._tail

    def add_first(self, value):
        if self._count == 0:
            self._tail = self._head = LinkedListNode(value)
            self._count += 1
        else:
            self._insert_before_head(value)

    def add_last(self, value):
        if self._count == 0:
            self._tail = self._head = LinkedListNode(value)
            self._count += 1
        else:
            self._insert_after_tail(value)

    def insert_after(self, node: LinkedListNode, value):
        if node == None:
            raise ValueError('Node argument cannot be None.')
        else:
            if node.next == None:
                self._insert_after_tail(value)
            else:
                node.next.prev = (new_node := LinkedListNode(value))
                new_node.next = node.next
                new_node.prev = node
                node.next = new_node
                self._count += 1


    def insert_before(self, node: LinkedListNode, value):
        if node == None:
            raise ValueError('Node argument cannot be None.')
        else:
            if node.prev == None:
                self._insert_before_head(value)
            else:
                node.prev.next = (new_node := LinkedListNode(value))
                new_node.prev = node.prev
                new_node.next = node
                node.prev = new_node
                self._count += 1

    def remove_first(self):
        if self._count == 0:
            raise ValueError('The DLL is empty. Cannot remove.')
        if self._count == 1:
            old_el = self._head
            self._head = self._tail = None
            self._count = 0
            return old_el.value
        else:
            old_head = self._head
            self._head = self._head.next
            self._head.prev = None
            self._count -= 1
            return old_head.value

    def remove_last(self):
        if self._count == 0:
            raise ValueError('The DLL is empty. Cannot remove.')
        if self._count == 1:
            old_el = self._head
            self._head = self._tail = None
            self._count = 0
            return old_el.value
        else:
            old_tail = self._tail
            self._tail = self._tail.prev
            self._tail.next = None
            self._count -= 1
            return old_tail.value

    def find(self, value):
        head = self._head
        while head:
            if head.value == value:
                return head
            head = head.next
        
        return None

    def values(self):
        values = []
        head = self._head
        while head:
            values.append(head.value)
            head = head.next
        
        return tuple(values)

    def _insert_before_head(self, value):
        if self._count == 0:
            raise ValueError('The DLL is empty, there is no head.')
        self._head.prev = (new_node := LinkedListNode(value))
        new_node.next = self._head
        self._head = new_node
        self._count += 1

    def _insert_after_tail(self, value):
        if self._count == 0:
            raise ValueError('The DLL is empty, there is no tail.')
        self._tail.next = (new_node := LinkedListNode(value))
        new_node.prev = self._tail
        self._tail = new_node
        self._count += 1
