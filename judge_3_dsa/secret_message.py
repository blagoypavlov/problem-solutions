class ListNode:
    def __init__(self, value, next=None):
        self.value = value
        self.next: ListNode = next

class Stack:
    def __init__(self):
        self._top: ListNode = None

    def push(self, value):
        self._top = ListNode(value, self._top)

    def pop(self):
        val = self._top.value
        if self._top:
            self._top = self._top.next

        return val

    def peek(self):
        return self._top.value

    @property
    def is_empty(self):
        return self._top is None
    
    def __str__(self):
        stack = ''
        while self._top:
            stack = self.pop() + stack
        return stack



def decrypt(encrypted: str):
    stack_1 = Stack()
    stack_2 = Stack()
    decrypted = ''
    times = ''
    if encrypted == '':
        return ''
    for char in encrypted:
        if char.isalpha() and stack_1.is_empty:
            decrypted += char
        elif char.isalpha() and not stack_1.is_empty:
            stack_2.push(char)
        elif char.isnumeric() and stack_1.is_empty:
            times += char
        elif char.isnumeric() and not stack_1.is_empty:
            stack_2.push(char)
        elif char == '{':
            if not stack_1.is_empty:
                stack_2.push(char)
            stack_1.push(char)
        elif char == '}':
            stack_1.pop()
            if not stack_1.is_empty:
                stack_2.push(char)
            else:
                to_be_decrypted = str(stack_2)
                decrypted += int(times) * decrypt(to_be_decrypted)
                times = ''
    return decrypted

    
input_ = input()
print(decrypt(input_))
