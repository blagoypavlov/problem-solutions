class Node:
    curr_id = 0

    @classmethod
    def gen_id(cls):
        cls.curr_id += 1
        return cls.curr_id

    def __init__(self, val: int):
        self.id = self.gen_id()
        self.val = val


class FreqStack:
    def __init__(self) -> None:
        self.nodes: dict[int, list[Node]] = {}
        # order!!

    @property
    def pairs_sorted(self):
        return sorted(
            list(self.nodes.items()), reverse=True, key=lambda pair: len(pair[1])
        )

    def push(self, val: int):
        self.top = (new_node := Node(val))
        if val in self.nodes:
            self.nodes[val].append(new_node)
        else:
            self.nodes[val] = [new_node]

    def pop(self):
        pairs_sorted = self.pairs_sorted
        if len(pairs_sorted[0][1]) != len(pairs_sorted[1][1]):
            target_node = self.nodes[pairs_sorted[0][0]].pop()
            return target_node.val
        else:
            target_pair = pairs_sorted[0]
            for idx in range(1, len(pairs_sorted)):
                if len(pairs_sorted[idx][1]) < len(target_pair[1]):
                    break
                if pairs_sorted[idx][1][-1].id > target_pair[1][-1].id:
                    target_pair = pairs_sorted[idx]
            target_node = self.nodes[target_pair[0]].pop()
            return target_node.val


# dict_ = {1: 2, 3: 4}
# print(list(dict_.items()))

st = FreqStack()

st.push(5)
# print(st.pairs_sorted)
st.push(7)
# print(st.pairs_sorted)

st.push(5)
# print(st.pairs_sorted)
st.push(7)
# print(st.pairs_sorted)

st.push(4)
# print(st.pairs_sorted)
st.push(5)
# print(st.pairs_sorted)

print(st.pop())
# print(st.pairs_sorted)
print(st.pop())
# print(st.pairs_sorted)
print(st.pop())
# print(st.pairs_sorted)
print(st.pop())
# print(st.pairs_sorted)
