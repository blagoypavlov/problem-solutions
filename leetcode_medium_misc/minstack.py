from typing import Optional


class Node:
    def __init__(self, val: int, next: "Node" = None) -> None:
        self.val = val
        self.next = next
        self.smaller = None


class MinStack:
    def __init__(self):
        self.top_: Optional[Node] = None
        self.min: Optional[Node] = None

    def push(self, val: int) -> None:
        self.top_ = (new_node := Node(val, self.top_))
        if self.min == None:
            self.min = new_node
            return
        if self.min.val >= new_node.val:
            new_node.smaller = self.min
            self.min = new_node

    def pop(self) -> None:
        if self.top_:
            if self.top_ == self.min:
                self.min = self.top_.smaller
            self.top_ = self.top_.next

    def top(self) -> int:
        if self.top_:
            return self.top_.val

    def getMin(self) -> int:
        if self.min:
            return self.min.val
