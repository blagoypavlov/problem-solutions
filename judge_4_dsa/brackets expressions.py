def math_expr(inp: list):
    results = []
    try:
        end = inp.index(')')
    except ValueError:
        return []
    for idx in range(end-1, -1, -1):
        if inp[idx] == '(':
            start = idx
            break
    results.append((start, end))
    inp[start] = '['
    inp[end] = ']'
    results += math_expr(inp)
    return results

initial_str = input()
result = math_expr(list(initial_str))

for el in result:
    print(initial_str[el[0]:el[1]+1])



# 5 * (123 * (1 + 3) + ((4 - 3) / 6))