class Solution:
    def reverse(self, x: int) -> int:
        chars = str(x)
        result = ""
        neg = False

        if x == 0:
            return 0

        if chars[0] == "-":
            neg = True
            chars = chars[1:]

        while chars[0] == "0":
            chars = chars[1:]

        for ch in chars:
            result = ch + result

        if neg:
            result = "-" + result

        final_int = int(result)

        return final_int if (-(2**31) <= final_int <= 2**31 - 1) else 0
