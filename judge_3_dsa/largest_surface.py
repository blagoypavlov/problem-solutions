def build_matrix(n):
    matrix = []
    for _ in range(n):
        matrix.append([int(num) for num in input().split()])
    return matrix

def in_matrix_not_seen(matrix, r, c) -> bool:
    rows = len(matrix) - 1
    cols = len(matrix[0]) - 1
    if 0 <= r <= rows and 0 <= c <= cols and matrix[r][c] != None:
        return True
    return False

def biggest_field(matrix, r, c, val):
    count = 0
    if matrix[r][c]:
        count += 1
        matrix[r][c] = None
    else:
        return 0
    left = in_matrix_not_seen(matrix, r, c - 1) and matrix[r][c - 1] == val
    right = in_matrix_not_seen(matrix, r, c + 1) and matrix[r][c + 1] == val
    up = in_matrix_not_seen(matrix, r - 1, c) and matrix[r - 1][c] == val
    down = in_matrix_not_seen(matrix, r + 1, c) and matrix[r + 1][c] == val
    if left:
        count += biggest_field(matrix, r, c - 1, val)
    if right:
        count += biggest_field(matrix, r, c + 1, val)
    if up:
        count += biggest_field(matrix, r - 1, c, val)
    if down:
        count += biggest_field(matrix, r + 1, c, val)
    
    return count

dict_counts = {}
N, _ = (int(num) for num in input().split())
matrix = build_matrix(N)

for r in range(len(matrix)):
    for c in range(len(matrix[0])):
        value = matrix[r][c]
        result = biggest_field(matrix, r, c, value)
        if result != 0:
            if value in dict_counts:
                if result > dict_counts[value]:
                    dict_counts[value] = result
            else:
                dict_counts[value] = result


print(sorted(dict_counts.items(), key=lambda t: t[1], reverse=True)[0][1])