# words = input().split()

# all_dicts: list[dict] = []
# result: list[str] = []

# def wrd_to_dict(wrd: str):
#     dict_ = {}
#     for char in sorted(wrd):
#         if char in dict_:
#             dict_[char] += 1
#         else:
#             dict_[char] = 1
#     return dict_

# for wrd in words:
#     if (new_dict := wrd_to_dict(wrd)) not in all_dicts:
#         all_dicts.append(new_dict)
#         result.append(wrd)
    
# print(' '.join(result))

# words = input().split()

# seen: list[list] = []
# result: list[str] = []

# for wrd in words:
#     if (sorted_wrd := sorted(wrd)) not in seen:
#         seen.append(sorted_wrd)
#         result.append(wrd)
    
# print(' '.join(result))

words = input().split()

seen: set[str] = set()

for wrd in words:
    if (sorted_wrd := ''.join(sorted(wrd))) not in seen:
        seen.add(sorted_wrd)
        print(wrd, end=' ')


