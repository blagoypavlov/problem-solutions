class Solution:
    def __init__(self) -> None:
        self.count = 0

    def _count_em(self, m: int, n: int, cur_m: int, cur_n: int):
        if cur_m == 0 and cur_n == n - 1:
            self.count += 1
            return

        elif cur_m == 0:
            # right
            self._count_em(m, n, cur_m, cur_n + 1)
            return

        elif cur_n == n - 1:
            # down
            self._count_em(m, n, cur_m - 1, cur_n)
            return

        self._count_em(m, n, cur_m - 1, cur_n)
        self._count_em(m, n, cur_m, cur_n + 1)

    def uniquePaths(self, m: int, n: int) -> int:
        self._count_em(m, n, m - 1, 0)
        return self.count
