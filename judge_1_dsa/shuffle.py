class Node:
    def __init__(self, value, prev = None, next = None):
        self.value = value
        self.next = next
        self.prev = prev
    

class LinkedList:
    def __init__(self):
        self._count = 0
        self._head = None
        self._tail = None

    # destroys the list
    def __str__(self) -> str:
        lst_output = []
        while self._head:
            lst_output.append(str(self.remove_node(self._head)))
        
        return ' '.join(lst_output)

    def insert_after(self, node: Node, new: Node):
        # if not self._head:
        #     raise ValueError('List is empty')
        if node.next == None:
            new.next = None
            new.prev = node
            node.next = new
            self._tail = new
            self._count += 1
        else:
            new.prev = node
            new.next = node.next
            node.next.prev = new
            node.next = new
            self._count += 1
        
        return new
    
    def add_last_instantiate(self, value):
        if self._head:
            self._tail.next = (new_node := Node(value, prev = self._tail))
            self._tail = new_node
            self._count += 1
        else:
            self._head = self._tail = (new_node := Node(value))
            self._count += 1
        
        return new_node
    
    def remove_node(self, node: Node):
        # if not self._head:
        #     raise ValueError('List is empty') 
        if self._count == 1:
            self._head = self._tail = None
            self._count = 0
        elif not node.prev:
            self._head = node.next
            self._head.prev = None
            self._count -= 1
        elif not node.next:
            self._tail = node.prev
            self._tail.next = None
            self._count -= 1
        else:
            node.next.prev = node.prev
            node.prev.next = node.next
            self._count -= 1
        
        return node.value

N, K = [int(inp) for inp in input().split()]

lst_numbers_for_shuffle = [int(inp) for inp in input().split()]

linked_list = LinkedList()
dict_obj = {}

for num in range(1, N+1):
    dict_obj[num]: dict[str, Node] = linked_list.add_last_instantiate(num)

for shuff_num in lst_numbers_for_shuffle:
    shuffled_node = dict_obj[shuff_num]
    if shuff_num % 2 == 0:
        target_node = dict_obj[int(shuff_num / 2)]
    elif shuff_num == N and shuff_num % 2 != 0:
        continue
    elif shuff_num % 2 != 0:
        target_node = dict_obj[(shuff_num * 2) if (shuff_num * 2) < N else N]
    linked_list.remove_node(shuffled_node)
    linked_list.insert_after(target_node, shuffled_node)

print(linked_list)