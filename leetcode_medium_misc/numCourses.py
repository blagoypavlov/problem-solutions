from typing import List


class Solution:
    def deps_coherent(self, num: int, requirements: dict, curr_reqs: list = None):
        if curr_reqs == None:
            curr_reqs = requirements[num]

        if num in curr_reqs:
            return False

        for el in curr_reqs:
            return self.deps_coherent(num, requirements[el], requirements)

        return True

    def canFinish(self, numCourses: int, prerequisites: List[List[int]]) -> bool:
        courses = [num for num in range(numCourses)]
        requirements = {num: [] for num in courses}

        # dict[number, its deps]

        for pair in prerequisites:
            requirements[pair[0]].append(pair[1])
        full_reqs = requirements.copy()

        while True:
            temp_possible = [el for el in courses if self.deps_coherent(el, full_reqs)]
            if temp_possible == []:
                return False
            for course in temp_possible:
                for key, val in requirements.items():
                    if course == key:
                        for key, val in requirements.items():
                            if course in val:
                                val.remove(course)
                        requirements.pop(course)
                        courses.remove(course)
                        break

            if courses == []:
                return True
