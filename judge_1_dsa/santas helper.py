class Wish:
    def __init__(self, item: str, price: float, child_name: str):
        self._item = item
        self._price = price
        self._child_name = child_name

    def __eq__(self, __o: object) -> bool:
        if not isinstance(__o, Wish):
            return False
        if self.params == __o.params:
            return True
        return False
    
    def __str__(self) -> str:
        return '{' + f'{self._item};{self._child_name};{self._price:.2f}' + '}'
    
    @property
    def price(self):
        return self._price
    
    @property
    def params(self):
        return tuple(self._item, self._price, self._child_name)
    

children_dict: dict[str, Wish] = {}
price_dict: dict[float, Wish] = {}

def add_wish(params):
    item, price, child_name = params
    price = float(price)
    if child_name not in children_dict:
        children_dict[child_name] = [(new_wish := Wish(item, price, child_name))]
    else:
        children_dict[child_name].append((new_wish := Wish(item, price, child_name)))
    if price not in price_dict:
        price_dict[price] = [new_wish]
    else:
        price_dict[price].append(new_wish)
    print('Wish added')

def delete_wishes(params):
    child_name = params[0]
    if child_name not in children_dict:
        print('No Wishes found')
    else:
        count_wishes = len(children_dict[child_name])
        for wish in children_dict[child_name]:
            price_dict[wish.price].remove(wish)
        del children_dict[child_name]
        print(f'{count_wishes} Wishes deleted')

def find_wishes_pricerange(params):
    low, high = params
    low = float(low)
    high = float(high)
    wishes_output = []
    for price, wishlist in price_dict.items():
        if low <= price <= high:
            wishes_output += wishlist
    if not wishes_output:
        print('No Wishes found')
    else:
        print('\n'.join(sorted(str(wish) for wish in wishes_output)))

def find_wishes_child(params):
    child_name = params[0]
    if child_name not in children_dict:
        print('No Wishes found')
    else:
        print('\n'.join(sorted([str(wish) for wish in children_dict[child_name]])))


def line_execute(input_line: str):
    for idx in range(len(input_line)):
        if input_line[idx] == ' ':
            cmd = input_line[ : idx]
            params = input_line[idx+1 : ].split(';')
            break

    if cmd.lower() == 'addwish':
        add_wish(params)
            
    elif cmd.lower() == 'deletewishes':
        delete_wishes(params)
    
    elif cmd.lower() == 'findwishesbypricerange':
        find_wishes_pricerange(params)
                    
    elif cmd.lower() == 'findwishesbychild':
        find_wishes_child(params)
            


N = int(input())

for _ in range(N):
    line_execute(input())
