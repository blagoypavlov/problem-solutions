from typing import List


class Solution:
    def bin_srch(self, nums: List[int], target: int, start: int, end: int):
        mid = (start + end) // 2
        if start > end:
            return -1
        if nums[mid] == target:
            return mid
        elif nums[mid] > target:
            return self.bin_srch(nums, target, start=start, end=mid - 1)
        else:
            return self.bin_srch(nums, target, start=mid + 1, end=end)

    def find_target(self, nums: List[int], target: int):
        first = 0
        second = len(nums) - 1
        while second >= first:
            if nums[first] == target:
                return first
            elif nums[second] == target:
                return second
            else:
                first += 1
                second -= 1
        return -1

    def search(self, nums: List[int], target: int) -> int:
        if nums[0] < nums[len(nums) - 1]:
            return self.bin_srch(nums, target, start=0, end=len(nums) - 1)
        else:
            return self.find_target(nums, target)
