from typing import Optional


class Node:
    def __init__(self, x: int, next: "Node" = None, random: "Node" = None):
        self.val = int(x)
        self.next = next
        self.random = random


class Solution:
    def _create_new_ll(self, head: Optional[Node]) -> Optional[Node]:
        # dict[old_node, new_node]
        lst_old = []
        lst_new = []

        if not head:
            return None

        new_head = Node(head.val)
        lst_new.append(new_head)
        lst_old.append(head)
        head = head.next
        prev_node = new_head

        while head:
            prev_node.next = (new_node := Node(head.val))
            prev_node = new_node
            lst_new.append(new_node)
            lst_old.append(head)
            head = head.next

        return new_head, lst_new, lst_old

    def copyRandomList(self, head: Optional[Node]) -> Optional[Node]:
        if not head:
            return None

        new_head, lst_new, lst_old = self._create_new_ll(head)
        curr_new = new_head

        for _ in range(len(lst_old)):
            if not head.random:
                curr_new.random = None
                curr_new = curr_new.next
                head = head.next
                continue
            curr_new.random = lst_new[lst_old.index(head.random)]
            curr_new = curr_new.next
            head = head.next

        return new_head
