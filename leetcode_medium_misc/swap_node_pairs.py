# Definition for singly-linked list.
from typing import Optional


class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


class Solution:
    def swapPairs(self, head: Optional[ListNode]) -> Optional[ListNode]:
        if not head:
            return
        if not head.next:
            return head

        result = head.next
        while head:
            if not head.next:
                break
            new_head = head.next
            old_head = head
            head = new_head.next
            new_head.next = old_head
            old_head.next = head.next if head and head.next else head

        return result


s = Solution()
lst = ListNode(1, ListNode(2))
new = s.swapPairs(lst)

while new:
    print(new.val)
    new = new.next
