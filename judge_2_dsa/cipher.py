def create_map_dict(map_input: str):
    list_digits = []
    list_letters = []
    map_dict = {}
    str_digits = ''
    for ch in map_input:
        if ch.isalpha():
            if str_digits:
                list_digits.append(str_digits)
            str_digits = ''
            list_letters.append(ch)
        elif ch.isdigit():
            str_digits += ch
    list_digits.append(str_digits)
    for idx in range(len(list_digits)):
        map_dict[list_digits[idx]] = list_letters[idx]

    return map_dict

def concatenate_variants(head: str, lst_tails: list[str]):
    vars = []
    if lst_tails == ['+']:
        return head
    if lst_tails:
        for tail in lst_tails:
            vars.append(head+tail)
        return vars
    else:
        return []






def decipher(ciphered: str, map_dict: dict[str, str], idx = 0) -> list[str]:
    variations = []
    if ciphered == '':
        return ['+']
    if idx == len(ciphered):
        return []
    if ciphered[:idx+1] in map_dict:
        unciphered = map_dict[ciphered[:idx+1]]
        still_ciphered = ciphered[idx+1:]
        variations += concatenate_variants(unciphered, decipher(still_ciphered, map_dict))
    variations += decipher(ciphered, map_dict, idx+1)
    return variations






ciphered_msg = input()
map_ = input()
map_ = create_map_dict(map_)
result = sorted(decipher(ciphered_msg, map_))
if result:
    print(len(result))
    print('\n'.join(result))
else:
    print(0)