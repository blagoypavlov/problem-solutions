# with data saving
def find_variations(length, a, b, curr_str = ''):
    variations = []
    if len(curr_str) == length:
        variations.append(curr_str)
        return variations
    variations += find_variations(length, a, b, curr_str + a)
    variations += find_variations(length, a, b, curr_str + b)
    return variations


length = int(input())
a, b = input().split()
print('\n'.join(sorted(find_variations(length, a, b))))


# # without saving data - printing directly
# def find_variations(length, a, b, curr_str = ''):
#     if len(curr_str) == length:
#         print(curr_str)
#         return
#     find_variations(length, a, b, curr_str + a)
#     find_variations(length, a, b, curr_str + b)

# length = int(input())
# a, b = sorted(input().split())
# find_variations(length, a, b)