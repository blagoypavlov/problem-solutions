class ListNode:
    def __init__(self, value, next=None):
        self.value = value
        self.next: ListNode = next

class Stack:
    def __init__(self):
        self._top: ListNode = None

    def push(self, value):
        self._top = ListNode(value, self._top)

    def pop(self):
        val = self._top.value
        if self._top:
            self._top = self._top.next

        return val

    def peek(self):
        return self._top.value

    @property
    def is_empty(self):
        return self._top is None
    
    def __str__(self):
        stack = ''
        while self._top:
            stack = self.pop() + stack
        return stack

HDNL_lines = [input() for _ in range(int(input()))]

def sep_lett_digits(inp: str):
    for idx in range(len(inp)):
        if inp[idx].isnumeric():
            return inp[:idx], int(inp[idx:])

HDNL_lines = [sep_lett_digits(line) for line in HDNL_lines]

def to_str_tup(inp: tuple, close = False):
    if not close:
        return '<' + inp[0] + str(inp[1]) + '>'
    else:
        return '</' + inp[0] + str(inp[1]) + '>'

s = Stack()
indent = 0

for line in HDNL_lines:
    if s.is_empty:
        indent = 0
        print(' '*indent + to_str_tup(line))
        s.push((indent, (line)))
    elif line[1] > s.peek()[1][1]:
        indent += 1
        print(' '*indent + to_str_tup(line))
        s.push((indent, (line)))
    else:
        while line[1] <= s.peek()[1][1]:
            popped = s.pop()
            indent = popped[0]
            print(' '*indent + to_str_tup(popped[1], close=True))
            if s.is_empty:
                break
        print(' '*indent + to_str_tup(line))
        s.push((indent, (line)))
    
while not s.is_empty:
    popped = s.pop()
    indent = popped[0]
    print(' '*indent + to_str_tup(popped[1], close=True))
